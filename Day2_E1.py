"""
1.Familiarize yourself with the data set below.
2.Make a plan for how you want to approach this task. What sub-tasks need to be done?
  - Read file
  - mainfunc that takes coord as input and returns either water or land output
  -
3.Estimate how much time each sub-task will take you before starting.
  - Read file: 5-10 min
  - mainfunc: 10-15 min:
  -
4.Make frequent git commits while you work on the solution.data
5.You can work together on the concepts, but everyone should write their own individual copy of code
"""


import numpy as np
import matplotlib.pyplot as plt


def read_data_and_adjust(
    filenamepath="./bin/gl-latlong-1km-landcover.bsq",
    pixels=43200,  # info from gl0500bs.txt
    lines=21600,  # info from gl0500bs.txt
    UpLeftX=-180,  # info from gl0500bs.txt
    UpLeftY=90,  # info from gl0500bs.txt
    LoRightX=180,  # info from gl0500bs.txt
    LoRightY=-90,  # info from gl0500bs.txt
):

    dlat = np.abs(UpLeftY - LoRightY) / lines
    dlon = np.abs(UpLeftX - LoRightX) / pixels

    data = np.fromfile(filenamepath, dtype="uint8")
    if data.shape != [lines, pixels]:
        data = data.reshape(lines, pixels)

    lons = np.arange(UpLeftX, LoRightX, dlon)
    lats = np.arange(LoRightY, UpLeftY, dlat)[::-1]

    return data, lons, lats


def plot_data(data, lons, lats, pointlonlat=None, zoom=True):
    fig, ax = plt.subplots(1, 1, figsize=(7, 9))
    ax.contourf(lons[::50], lats[::50], data[::50, ::50])
    if pointlonlat:
        ax.plot(lons[pointlonlat[0]], lats[pointlonlat[1]], "ro")
        if zoom:
            ax.set_xlim(lons[pointlonlat[0]] - 20, lons[pointlonlat[0]] + 20)
            ax.set_ylim(lats[pointlonlat[1]] - 20, lats[pointlonlat[1]] + 20)
    plt.show()


def nearest_point_in_data(lonlat, data, datalons, datalats):
    dlon = np.abs(lonlat[0] - datalons)
    dlat = np.abs(lonlat[1] - datalats)
    lon_nearestidx = np.where(np.isin(dlon, dlon.min()))
    lat_nearestidx = np.where(np.isin(dlat, dlat.min()))
    lonlat_nearest = [lon_nearestidx, lat_nearestidx]
    return lonlat_nearest


def is_point_on_land_or_water(lonlat_nearest, data):
    datavalue = data[lonlat_nearest[1], lonlat_nearest[0]]
    landorwater = "land"
    if datavalue == 0:
        landorwater = "water"
    return landorwater



# options of points:
lonlat = [9.13, 48.95]
# lonlat = [5.331850,60.383669] #bergen
# lonlat = [15.5015, 78.2453]  # Svalbard


data, datalons, datalats = read_data_and_adjust()

lonlat_nearest = nearest_point_in_data(
    lonlat=lonlat, data=data, datalons=datalons, datalats=datalats
)
landorwater = is_point_on_land_or_water(lonlat_nearest, data=data)
nice_lonlat = f"The coordinate of {lonlat[1]} N, {lonlat[0]}W" if lonlat[0]>0 else  f" The coordinate of {lonlat[1]}N, {lonlat[0]}E"
print(f"{nice_lonlat} is on {landorwater}")
plot_data(data, datalons, datalats, lonlat_nearest, zoom=True)
